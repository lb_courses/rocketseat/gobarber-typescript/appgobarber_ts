yarn add eslint -D

yarn add -D eslint-plugin-react@^7.20.0 @typescript-eslint/eslint-plugin@latest eslint-config-airbnb@latest  eslint-plugin-import@^2.21.2 eslint-plugin-jsx-a11y@^6.3.0 eslint-plugin-react-hooks@^4 @typescript-eslint/parser@latest

yarn add prettier eslint-config-prettier eslint-plugin-prettier  -D

yarn add styled-components
yarn add @types/styled-components

yarn add @react-navigation/native

yarn add react-native-reanimated react-native-gesture-handler react-native-screens react-native-safe-area-context @react-native-community/masked-view

<!-- navegaao por pilha -->
yarn add @react-navigation/stack

<!-- informa que estamos usando fonts -->
npx react-native link

<!-- ICONES -->
yarn add react-native-vector-icons
yarn add @types/react-native-vector-icons

<!-- ajuda a identificar o css -->
yarn add react-native-iphone-x-helper

yarn add @unform/core @unform/mobile

yarn add yup
yarn add @types/yup

yarn add @react-native-community/async-storage
